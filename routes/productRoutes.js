const express = require('express')
const router = express.Router()
const productController = require('../controllers/productControllers')
const auth = require('../auth')

//Specific Requirement 6: Create Product (Admin only)
router.post('/add', auth.verify, (req,res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	data.isAdmin ? productController.addProduct(data).then(result=>res.send(result))
	: res.send(false)
})

//Specific Requirement 7: Update Product Information (Admin only)
router.put('/:productId', auth.verify, (req,res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	data.isAdmin ? productController.updateProduct(req.params, data).then(result=>res.send(result)) 
	: res.send(false)
})

//Specific Requirement 4: Retrieve all active products
router.get('/', (req,res)=>{
	productController.getAllActive().then(result=>res.send(result))
})

//Specific Requirement 5: Retrieve single product
router.get('/find', (req,res)=>{
	productController.getOneProduct(req.body).then(result=>res.send(result))
})

//Specific Requirement 8: Archive Product (Admin only)
router.put('/:productId/archive', auth.verify, (req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	data.isAdmin ? productController.archiveProduct(req.params, data).then(result=>res.send(result)) 
	: res.send(false)
})

module.exports = router