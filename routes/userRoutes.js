const express = require('express')
const router = express.Router()
const userController = require('../controllers/userControllers')
const auth = require('../auth')

router.post('/checkEmail', (req,res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

//Specific Requirement 1: User Registration
router.post('/register', (req,res)=>{
	userController.registerUser(req.body).then(result => res.send(result))
})

//LOGIN WORKING
//Specific Requirement 2: User Authentication
router.post('/login', (req,res)=>{
	userController.loginUser(req.body).then(result => res.send(result))
})


router.get('/details', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(result => res.send(result))
})

//deleted /enroll

//Specific Requirement 3: Set user as admin (Admin only)
router.put('/:email', auth.verify, (req,res)=>{
	
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	data.isAdmin ? userController.promoteToAdmin(req.params).then(result=>res.send(result))
	: res.send(false)
})

module.exports = router