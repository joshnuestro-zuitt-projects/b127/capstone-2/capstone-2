const express = require('express')
const router = express.Router()
const orderController = require('../controllers/orderControllers')
const auth = require('../auth')

//Specific Requirement 9: Non-admin User checkout (Create Order) Part1 (Adding a Product to Cart)
//testing, adding items to a cart using productId
router.put('/:productId/cart', (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	orderController.addToCart(req.params,res,userData).then(result => res.send(result))
}) //update a cart
//oh my what am I doing? haha

//Specific Requirement 9: Non-admin User checkout (Create Order) Part2 (Checking Out)
router.put('/checkout', (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	orderController.toPay(userData).then(result => res.send(result))
})

//Specific Requirement 10: Retrieve all Orders (Admin only)
router.get('/', (req,res)=>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	data.isAdmin ? orderController.getAllOrders().then(result=>res.send(result))
	: res.send(false)
})

//Specific Requirement 11: Retrieve Authenticated User's Orders (NON-Admin only)
router.get('/history', (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	orderController.getMyOrders(userData).then(result=>res.send(result))
})

module.exports = router