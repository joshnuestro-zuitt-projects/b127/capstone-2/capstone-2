const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//Specific Requirement 9: Non-admin User checkout (Create Order) Part1 (Adding a Product to Cart)
//logging in creates a cart
//the reqParams is passed here as data to the 'items' in the 'order' model
//cartId is made of email and the first 15 characters of a date
module.exports.addToCart = (reqParams,res,data)=>{
	let date = new Date().toString()
	date = date.slice(0,15)

	let cartId = data.id + date

	return Product.findOne({_id: reqParams.productId})
	.then(result=>{
		let price = result.price


	// return Order.find({"items.productId": reqParams.productId}).then(result2=>{
	return Order.find({_id: cartId, items: {$elemMatch: {productId:reqParams.productId}} })
	.then(result2=>{

		if(result2.length > 0){
			// increases quantity by one everytime the product that already exists is sent again

			return Order.findOneAndUpdate(
			    {_id: cartId, items: {$elemMatch: {productId:reqParams.productId}} },
				{
					$inc: {
						"items.$.quantity": 1
					}
				}).then((saveCart, error)=>{
				return(error ? false : true)
				})
		}
		else{
			//adds product using productId only once
			    return Order.findByIdAndUpdate(
			    	{_id:cartId},
					{
						$push: {
							items: {
								"productId" : reqParams.productId,
								"amount": result.price,
								"quantity": 1
							}
						}
					},
					{new: true, upsert: true }).exec();
			        res.sendStatus(200).then((saveCart, error)=>{
					return(error ? false : true)
					})
			 
		}
	})
	})

 
}


//Specific Requirement 9: Non-admin User checkout (Create Order) Part2 (Checking Out)
//My attempt to do multiplication, instead I went to the corner and cried for just 3 hours
module.exports.toPay = (data)=>{
	let date = new Date().toString()
	date = date.slice(0,15)

	let cartId = data.id + date


	return Order.aggregate([
	{ $match: { _id:  cartId} },
	{
		$set: 
		{	
			totalAmount: {
				$sum: 				
				{

					$map: {
				        input: "$items",
				        as: "items",
				       	in: {
				       		$multiply: ["$$items.amount","$$items.quantity"]
			       		}
			    	}
				}
			}, 
			purchasedOn: new Date()
		}
	},
	{
		$addFields: {
			items: {
				$map: {
			        input: "$items",
			        as: "items",
			       	in: {
			            "productId": "$$items.productId",
			            "quantity": "$$items.quantity",
			            "price": "$$items.amount",
			            "subtotal": { 
			                $multiply: [
			                  "$$items.amount",
			                  "$$items.quantity"
			                ]
	              		} 
					}
				}
			}
		}
	}
	// ]).then(result=>{return result})
	]).then(result=>{

		let updatedCart = {
			totalAmount: result[0].totalAmount,
			purchasedOn: result[0].purchasedOn
		}

		return Order.findByIdAndUpdate(cartId, updatedCart).then((saveCart, error)=>{
		return(error ? false : result)
		})

	})
}

//Specific Requirement 10: Retrieve all Orders (Admin only)
module.exports.getAllOrders=()=>{
	return Order.find({}).then(result=>{return result})
}

// Specific Requirement 11: Retrieve Authenticated User's Orders (NON-Admin only)
module.exports.getMyOrders=(data)=>{
	return Order.find({userId:data.id}).then(result=>{return result})
}