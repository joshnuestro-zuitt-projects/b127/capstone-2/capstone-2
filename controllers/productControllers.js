const Product = require('../models/Product')

//Specific Requirement 6: Create Product (Admin only)
/*
Product creation looks like this:
{
	name: "Stik O",
	brand: "Ekko Food Corp.",
	category: "Snacks",
	description: "Highly addictive, avoid at all costs",
	price: 100
}
*/
module.exports.addProduct = (data)=>{
	let newProduct = new Product({
		name: data.product.name,
		brand: data.product.brand,
		category: data.product.category,
		description: data.product.description,
		price: data.product.price,
		image: data.product.image
		})


	return newProduct.save().then((product,error)=>{
		return(error ? false : true)
	})
}

//Specific Requirement 7: Update Product Information (Admin only)
module.exports.updateProduct = (reqParams, data) => {
	let updatedProduct = {
		name: data.product.name,
		brand: data.product.brand,
		category: data.product.category,
		description: data.product.description,
		price: data.product.price,
		image: data.product.image
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=>{
		return(error ? false : true)
	})
}

//Specific Requirement 4: Retrieve all active products
module.exports.getAllActive=()=>{
	return Product.find({isActive:true}).then(result=>{return result})
}

//Specific Requirement 5: Retrieve single product
//use "name":"<product name>"
module.exports.getOneProduct=(reqBody)=>{
	return Product.findOne({name:reqBody.name}).then(result=>{return result})
}

//Specific Requirement 8: Archive Product (Admin only)
module.exports.archiveProduct=(reqParams)=>{
	let updatedProduct = {
	isActive: false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error)=>{
		return(error ? false : true)
	})
}