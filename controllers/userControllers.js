const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//Specific Requirement 1: User Registration
//Now can check duplicate before logging in
//email becomes _id
/*
Registration looks like this:
{
	email: "email@email.com", //this becomes _id
	mobileNo: "0919",
	password: "hackme"
}
*/
module.exports.registerUser = (reqBody)=>{
	return User.find({_id:reqBody.email}).then(result=>{
		if(result.length > 0){
			return `
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣤⣤⣤⣤⣤⣶⣦⣤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀  ____________________________
⠀⠀⠀⠀⠀⠀⠀⠀⢀⣴⣿⡿⠛⠉⠙⠛⠛⠛⠛⠻⢿⣿⣷⣤⡀⠀⠀⠀⠀⠀/${reqBody.email}
⠀⠀⠀⠀⠀⠀⠀⠀⣼⣿⠋⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⠈⢻⣿⣿⡄⠀⠀⠀⠀ |𝐈𝐒 𝐒𝐔𝐒
⠀⠀⠀⠀⠀⠀⠀⣸⣿⡏⠀⠀⠀⣠⣶⣾⣿⣿⣿⠿⠿⠿⢿⣿⣿⣿⣄⠀⠀⠀| Register with a different email
⠀⠀⠀⠀⠀⠀⠀⣿⣿⠁⠀⠀⢰⣿⣿⣯⠁⠀⠀⠀⠀⠀⠀⠀⠈⠙⢿⣷⡄⠀/へ.＿＿＿＿＿＿＿______________
⠀⠀⣀⣤⣴⣶⣶⣿⡟⠀⠀⠀⢸⣿⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣷⠀
⠀⢰⣿⡟⠋⠉⣹⣿⡇⠀⠀⠀⠘⣿⣿⣿⣿⣷⣦⣤⣤⣤⣶⣶⣶⣶⣿⣿⣿⠀
⠀⢸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠹⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠃⠀
⠀⣸⣿⡇⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠉⠻⠿⣿⣿⣿⣿⡿⠿⠿⠛⢻⣿⡇⠀⠀
⠀⣿⣿⠁⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣧⠀⠀
⠀⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀
⠀⣿⣿⠀⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀
⠀⢿⣿⡆⠀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⡇⠀⠀
⠀⠸⣿⣧⡀⠀⣿⣿⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⠃⠀⠀
⠀⠀⠛⢿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⣰⣿⣿⣷⣶⣶⣶⣶⠶⢠⣿⣿⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⣽⣿⡏⠁⠀⠀⢸⣿⡇⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⣿⣿⡇⠀⢹⣿⡆⠀⠀⠀⣸⣿⠇⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢿⣿⣦⣄⣀⣠⣴⣿⣿⠁⠀⠈⠻⣿⣿⣿⣿⡿⠏⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠈⠛⠻⠿⠿⠿⠿⠋⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
			`
			// return true
		}
		else{
				let newUser = new User({
					_id: reqBody.email,
					mobileNo: reqBody.mobileNo,
					password: bcrypt.hashSync(reqBody.password, 10) 
				})
				return newUser.save().then((user,error)=>{
					return(error ? false : true)
				})
			
		}
	})
}


//LOGIN EXPERIMENT
//Create a temporary Cart in the Order Collection when Logging In
//Specific Requirement 2: User Authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({_id:reqBody.email}).then(result=>{
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect && result.isAdmin == false){
				let date = new Date().toString()
				date = date.slice(0,15)
				let newOrder = new Order({
						_id: `${reqBody.email}${date}`, //maximum character: 105 | original token: 155
						userId: reqBody.email
					})
					return newOrder.save().then((order,error)=>{
						return(error ? false : {accessToken:auth.createAccessToken(result.toObject())} )
					})

			}
			else if(isPasswordCorrect && result.isAdmin == true){
				return {accessToken:auth.createAccessToken(result.toObject())}
			}
			else{
				return false
			} 
		}
	})
}


//LOGIN WORKING
//Specific Requirement 2: User Authentication
// module.exports.loginUser = (reqBody) => {
// 	return User.findOne({_id:reqBody.email}).then(result=>{
// 		if(result == null){
// 			return false
// 		}
// 		else{
// 			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
// 			if(isPasswordCorrect){
// 				return {accessToken:auth.createAccessToken(result.toObject())}
// 			}
// 			else{
// 				return false
// 			} 
// 		}
// 	})
// }

//s33 maam judy version
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		result.password = "";
		return result;
	})
}

//Specific Requirement 3: Set user as admin (Admin only)
module.exports.promoteToAdmin=(reqParams)=>{
	let toAdmin = {
	isAdmin: true
	}

	return User.findByIdAndUpdate(reqParams.email, toAdmin).then((user, error)=>{
		return(error ? false : true)
	})
}