const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

const app = express()

mongoose.connect('mongodb+srv://admin:admin@zuitt-bootcamp.vlaci.mongodb.net/Batch127_Josh_Capstone2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', ()=>console.log('Now connected to MongoDB Atlas'))

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)

//my favorite piece of code
app.listen(process.env.PORT || 4001, ()=>{
	console.log(`
   @ ${process.env.PORT || 4001}
 _           _       _     _ ____ _____     _           _     
| |__   __ _| |_ ___| |__ / |___ \\___  |   (_) ___  ___| |__  
| '_ \\ / _\` | __/ __| '_ \\| | __) | / /    | |/ _ \\/ __| '_ \\ 
| |_) | (_| | || (__| | | | |/ __/ / /     | | (_) \\__ \\ | | |
|_.__/ \\__,_|\\__\\___|_| |_|_|_____/_/     _/ |\\___/|___/_| |_|
                                         |__/  
		`)
})

