const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
	_id: {
		type: String
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String
	},
	items: [
		{
			productId: {
				type: String
			},
			quantity: {
				type: Number
			},
			amount: {
				type: Number
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema)