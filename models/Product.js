const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true,"Product name is required"]
	},
	brand: {
		type: String,
		required: [true,"Brand is required"]
	},
	category: {
		type: String,
		required: [true,"Category is required"]
	},
	description: {
		type: String,
		required: [true,"Description is required"]
	},
	price: {
		type: Number,
		required: [true,"Price is required"]
	},
	image: {
		type: String,
		required: [true,"Image is required"]
	}
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	rating: {
		type: Number,
		default: 0
	},
	reviewers: {
		type: Number,
		default: 0
	},
	stock: {
		type: Number,
		default: 0
	},
	sold: {
		type: Number,
		default: 0
	}
})

module.exports = mongoose.model("Product", productSchema)